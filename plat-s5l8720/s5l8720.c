#include "aes.h"
#include "arm/arm.h"
#include "clock.h"
#include "dma.h"
#include "event.h"
#include "framebuffer.h"
#include "gpio.h"
#include "interrupt.h"
#include "i2c.h"
#include "pmu.h"
#include "lcd.h"
#include "miu.h"
#include "mmu.h"
#include "openiboot.h"
#include "power.h"
#include "spi.h"
#include "tasks.h"
#include "timer.h"
#include "uart.h"
#include "wdt.h"
#include "audiocodec.h"

// TODO: remove
#include "actions.h"
#include "buttons.h"
#include "images.h"
#include "mtd.h"

extern void _binary_plat_s5l8720_emcore_ipodtouch2g_bin_start;
extern void _binary_plat_s5l8720_emcore_ipodtouch2g_bin_size;

void load_iboot() {
	framebuffer_clear();
	bufferPrintf("Loading iBoot...\r\n");
	Image* image = images_get(fourcc("ibox"));
	if (image == NULL) {
		bufferPrintf("using ibot\r\n");
		image = images_get(fourcc("ibot"));
		if (image == NULL) {
			bufferPrintf("no usable iboot image\r\n");
		}
	} else {
		bufferPrintf("using ibox\r\n");
	}
	void* imageData;
	images_read(image, &imageData);
	chainload((uint32_t)imageData);
}

void load_emcore() {
	framebuffer_clear();
	bufferPrintf("Loading emCORE...\r\n");
	memcpy((void *)0x09000000,
		&_binary_plat_s5l8720_emcore_ipodtouch2g_bin_start,
		(size_t) &_binary_plat_s5l8720_emcore_ipodtouch2g_bin_size);
	chainload(0x09000000);
}

// TODO: remove
static TaskDescriptor iboot_loader_task;

void iboot_loader_run(void) {
	uint64_t startTime = timer_get_system_microtime();
	// boot iboot when the up button is pressed
	// boot emCORE when the down button is pressed
	// poweroff when the hold button is pressed
	static Boolean messageShown = FALSE;
	while(1) {
		if (!gpio_pin_state(BUTTONS_VOLUP)) {
			load_iboot();
			task_stop();
		}
		if (!gpio_pin_state(BUTTONS_VOLDOWN)) {
			load_emcore();
			task_stop();
		}
		if (gpio_pin_state(BUTTONS_HOLD) && !gpio_pin_state(BUTTONS_HOME) && messageShown) {
			pmu_poweroff();
			task_stop();
		}
		if (has_elapsed(startTime, 2 * 1000 * 1000) && !messageShown) {
			// show a welcome message after 2 seconds to skip all of the usb spam
			bufferPrintf("===================\r\n");
			bufferPrintf("Welcome to the iPod Touch 2G experimental openiBoot!\r\n");
			bufferPrintf("Press the Volume up button to load iOS\r\n");
			bufferPrintf("Press the Volume down button to load emCORE\r\n");
			bufferPrintf("Press the Hold button to power off\r\n");
			bufferPrintf("===================\r\n");
			bufferPrintf("\r\n\r\n\r\n");
			messageShown = TRUE;
		}
		task_yield();
	}
}

void platform_init()
{
	arm_setup();
	mmu_setup();
	tasks_setup();

	// Basic prerequisites for everything else
	miu_setup();
	power_setup();

	clock_setup();

	// Need interrupts for everything afterwards
	interrupt_setup();
	
	gpio_setup();

	// For scheduling/sleeping niceties
	timer_setup();
	event_setup();
	wdt_setup();

	// Other devices
	uart_setup();
	i2c_setup();

	dma_setup();

	spi_setup();

	LeaveCriticalSection();

	aes_setup();

	displaypipe_init();
	framebuffer_setup();
	framebuffer_setdisplaytext(TRUE);
	lcd_set_backlight_level(186);

	// audiohw_init();
	
	// TODO: remove
	task_init(&iboot_loader_task, "iboot loader", TASK_DEFAULT_STACK_SIZE);
	task_start(&iboot_loader_task, &iboot_loader_run, NULL);
}

void platform_shutdown()
{
	dma_shutdown();
	wdt_disable();
	arm_disable_caches();
	mmu_disable();
}
