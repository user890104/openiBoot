#include "openiboot.h"
#include "buttons.h"
#include "hardware/buttons.h"
#include "pmu.h"
#include "gpio.h"

int buttons_is_pushed(int which) {
	if (!pmu_get_reg(BUTTONS_IIC_STATE)) {
		if (which == BUTTONS_VOLUP || which == BUTTONS_VOLDOWN) {
			return TRUE;
		}
		
		return FALSE;
	}

	if (gpio_pin_state(which)) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}
